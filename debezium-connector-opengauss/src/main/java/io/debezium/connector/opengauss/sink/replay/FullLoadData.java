package io.debezium.connector.opengauss.sink.replay;

import com.mysql.cj.jdbc.ClientPreparedStatement;
import com.mysql.cj.jdbc.exceptions.CommunicationsException;
import com.zaxxer.hikari.HikariDataSource;
import io.debezium.DebeziumException;
import io.debezium.connector.breakpoint.BreakPointObject;
import io.debezium.connector.breakpoint.BreakPointRecord;
import io.debezium.connector.opengauss.sink.object.ColumnMetaData;
import io.debezium.connector.opengauss.sink.object.ConnectionInfo;
import io.debezium.connector.opengauss.sink.object.DmlOperation;
import io.debezium.connector.opengauss.sink.object.SinkRecordObject;
import io.debezium.connector.opengauss.sink.object.SourceField;
import io.debezium.connector.opengauss.sink.object.TableMetaData;
import io.debezium.connector.opengauss.sink.utils.SqlTools;
import org.apache.kafka.connect.data.Struct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class FullLoadData {
    private static final Logger LOGGER = LoggerFactory.getLogger(FullLoadData.class);
    private static final String TRUNCATE = "t";
    private static final String PATH = "p";
    private static final String LOAD_SQL = "LOAD DATA LOCAL INFILE 'sql.csv' "
            + " INTO TABLE %s"
            + " FIELDS TERMINATED BY ','"
            + " enclosed by '\\''"
            + " LINES TERMINATED BY '%s' ";
    private static final String DELIMITER = System.lineSeparator();
    private static final int tableThreadNum= 2;

    Map<String, Integer> processRecordMap = new ConcurrentHashMap<>();
    private boolean isClearFile;
    private final DateTimeFormatter sqlPattern = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH:mm:ss.SSS");
    private final HikariDataSource hikariDataSource;
    private final BreakPointRecord breakPointRecord;
    private final ConnectionInfo connectionInfo;
    private final ThreadPoolExecutor poolExecutor;
    private final SqlTools sqlTools;
    private final Map<String, String> schemaMappingMap;
    private final PriorityBlockingQueue<Long> replayedOffsets;
    private final AtomicInteger successCount = new AtomicInteger(0);
    private final AtomicInteger failCount = new AtomicInteger(0);
    private final List<String> failSqlList = new ArrayList<>();
    private final Map<String, TableMetaData> oldTableMap = new HashMap<>();
    private final Map<String, List<ColumnMetaData>> fullTableColumnMap = new HashMap<>();
    private final Map<String, ThreadPoolExecutor> tableThreadPool = new HashMap<>();

    public FullLoadData(ConnectionInfo connectionInfo, int threadNum, Map<String, String> schemaMappingMap,
                        SqlTools sqlTools, BreakPointRecord breakPointRecord) {
        this.connectionInfo = connectionInfo;
        this.hikariDataSource = connectionInfo.initHikariDataSource(threadNum);
        this.poolExecutor = new ThreadPoolExecutor(threadNum, threadNum, 5,
                TimeUnit.SECONDS, new LinkedBlockingQueue<>());
        this.sqlTools = sqlTools;
        this.schemaMappingMap = schemaMappingMap;
        this.breakPointRecord = breakPointRecord;
        this.replayedOffsets = breakPointRecord.getReplayedOffset();
    }

    public void setClearFile(boolean isClearFile) {
        this.isClearFile = isClearFile;
    }

    public int getSuccessCount() {
        return successCount.get();
    }

    public int getFailCount() {
        return failCount.get();
    }

    public List<String> getFailSqlList() {
        return failSqlList;
    }

    public void clearFailSqlList() {
        failSqlList.clear();
    }

    public void shutDownThreadPool () {
        poolExecutor.shutdownNow();
    }

    public void processSinkMessage(SinkRecordObject sinkRecordObject) {
        SourceField sourceField = sinkRecordObject.getSourceField();
        DmlOperation dmlOperation = sinkRecordObject.getDmlOperation();
        String operation = dmlOperation.getOperation();
        String schemaName = schemaMappingMap.get(sourceField.getSchema());
        String tableFullName = schemaName + "." + sourceField.getTable();
        TableMetaData tableMetaData;
        if (oldTableMap.containsKey(tableFullName)) {
            tableMetaData = oldTableMap.get(tableFullName);
        } else {
            tableMetaData = sqlTools.getTableMetaData(schemaName, sourceField.getTable());
            oldTableMap.put(tableFullName, tableMetaData);
        }
        switch (operation) {
            case TRUNCATE:
                String sql = String.format(Locale.ROOT, "truncate table %s", tableMetaData.getTableFullName());
                try (Connection connection = hikariDataSource.getConnection();){
                    Statement statement = connection.createStatement();
                    statement.execute(sql);
                    successCount.incrementAndGet();
                    replayedOffsets.offer(sinkRecordObject.getKafkaOffset());
                    savedBreakPointInfo(sinkRecordObject);
                    tableThreadPool.put(tableFullName, new ThreadPoolExecutor(tableThreadNum, tableThreadNum, 5,
                            TimeUnit.SECONDS, new LinkedBlockingQueue<>()));
                } catch (SQLException exp) {
                    failCount.incrementAndGet();
                    failSqlList.add("-- " + sqlPattern.format(LocalDateTime.now()) + ": "
                            + sinkRecordObject.getSourceField() + System.lineSeparator()
                            + "-- " + exp.getMessage() + System.lineSeparator() + sql + System.lineSeparator());
                    LOGGER.error("SQL exception occurred in struct date {}", sinkRecordObject.getSourceField());
                    LOGGER.error("The error SQL statement executed is: {}", sql);
                    LOGGER.error("the cause of the exception is {}", exp.getMessage());
                }
                break;
            case PATH:
                poolExecutor.submit(() -> {
                    // Ensure that truncate table executes before data is imported.
                    while (!tableThreadPool.containsKey(tableFullName)) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    tableThreadPool.get(tableFullName).execute(() ->
                            processFullData(dmlOperation, tableFullName, tableMetaData, sinkRecordObject));
//                    processFullData(dmlOperation, tableFullName, tableMetaData, sinkRecordObject);
                });
                break;
            default:
                throw new DebeziumException("Invalid playback message");
        }

    }

    private void processFullData(DmlOperation dmlOperation, String tableFullName, TableMetaData tableMetaData,
                                 SinkRecordObject sinkRecordObject) {
        String columnString = dmlOperation.getColumnString();
        String loadSql = String.format(Locale.ROOT, LOAD_SQL, tableFullName, System.lineSeparator());
        loadSql = sqlTools.sqlAddBitCast(tableMetaData, columnString, loadSql);
        List<String> lineList;
        String path = dmlOperation.getPath();
        try {
            lineList = Files.lines(Paths.get(path))
                    .flatMap(line -> Arrays.stream(line.split(System.lineSeparator())))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            LOGGER.error("load csv file failure IO exception", e);
            return;
        }
        Struct after = dmlOperation.getAfter();
        List<ColumnMetaData> columnList = getColumnList(tableFullName, tableMetaData, columnString);
        List<String> list = sqlTools.conversionFullData(columnList, lineList, after);
        ByteArrayInputStream inputStream = getInputStream(list);
        try (Connection connection = hikariDataSource.getConnection();){
            loadData(connection, inputStream, loadSql);
            processRecordMap.merge(tableFullName, list.size(), Integer::sum);
            replayedOffsets.offer(sinkRecordObject.getKafkaOffset());
            savedBreakPointInfo(sinkRecordObject);
            clearCsvFile(path);
        } catch (CommunicationsException exp) {
            retryLoad(loadSql, inputStream, sinkRecordObject);
            processRecordMap.merge(tableFullName, list.size(), Integer::sum);
            clearCsvFile(path);
        } catch (SQLException exp) {
            failCount.incrementAndGet();
            failSqlList.add("-- " + sqlPattern.format(LocalDateTime.now()) + ": "
                    + sinkRecordObject.getSourceField() + System.lineSeparator()
                    + "-- " + exp.getMessage() + System.lineSeparator() + loadSql + System.lineSeparator());
            LOGGER.error("SQL exception occurred in struct date {}", sinkRecordObject.getSourceField());
            LOGGER.error("The error SQL statement executed is: {}", loadSql);
            LOGGER.error("the cause of the exception is {}", exp.getMessage());
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                LOGGER.error("inputStream close error", e);
            }
        }
    }

    private void loadData(Connection connection, InputStream inputStream, String loadSql) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute("set session SQL_LOG_BIN=0;");
        statement.execute("set session UNIQUE_CHECKS=0;");
        PreparedStatement preparedStatement = connection.prepareStatement(loadSql);
        if (preparedStatement.isWrapperFor(com.mysql.cj.jdbc.JdbcStatement.class)) {
            ClientPreparedStatement clientPreparedStatement = preparedStatement.unwrap(ClientPreparedStatement.class);
            connection.setAutoCommit(false);
            clientPreparedStatement.setLocalInfileInputStream(inputStream);
            clientPreparedStatement.executeUpdate();
            connection.commit();
            successCount.incrementAndGet();
        }
    }

    private void retryLoad(String sql, InputStream ins, SinkRecordObject sinkRecordObject) {
        try (Connection connection = connectionInfo.createMysqlConnection();) {
            loadData(connection, ins, sql);
            replayedOffsets.offer(sinkRecordObject.getKafkaOffset());
            savedBreakPointInfo(sinkRecordObject);
        } catch (SQLException e) {
            LOGGER.error("SQL exception occurred in work thread.", e);
        }
    }

    private void clearCsvFile(String path) {
        if (!isClearFile) {
            return;
        }
        File file = new File(path);
        if (!file.delete()) {
            LOGGER.info("clear csv file failure.");
        }
    }

    private List<ColumnMetaData> getColumnList(String tableFullName, TableMetaData tableMetaData, String columnString) {
        if (fullTableColumnMap.containsKey(tableFullName)) {
            return fullTableColumnMap.get(tableFullName);
        }
        List<ColumnMetaData> columnList = tableMetaData.getColumnList();
        List<ColumnMetaData> columnMetaDataList = new ArrayList<>();
        String[] columns = columnString.split(",");
        for (String column : columns) {
            for (ColumnMetaData columnMetaData : columnList) {
                if (columnMetaData.getColumnName().equals(column)) {
                    columnMetaDataList.add(columnMetaData);
                }
            }
        }
        fullTableColumnMap.put(tableFullName, columnMetaDataList);
        return columnMetaDataList;
    }

    private ByteArrayInputStream getInputStream(List<String> data) {
        String dataStr = String.join(DELIMITER, data) + DELIMITER;
        byte[] bytes = dataStr.getBytes(StandardCharsets.UTF_8);
        return new ByteArrayInputStream(bytes);
    }

    private void savedBreakPointInfo(SinkRecordObject sinkRecordObject) {
        BreakPointObject openGaussBpObject = new BreakPointObject();
        SourceField sourceField = sinkRecordObject.getSourceField();
        Long lsn = sourceField.getLsn();
        Long kafkaOffset = sinkRecordObject.getKafkaOffset();
        openGaussBpObject.setBeginOffset(kafkaOffset);
        openGaussBpObject.setLsn(lsn);
        openGaussBpObject.setTimeStamp(LocalDateTime.now().toString());
        breakPointRecord.storeRecord(openGaussBpObject, false, true);
    }
}
